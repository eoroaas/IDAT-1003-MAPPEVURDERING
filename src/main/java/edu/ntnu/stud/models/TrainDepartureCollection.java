package edu.ntnu.stud.models;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Stream;

/**
 * This class represents a collection of train departures.
 */
public class TrainDepartureCollection implements Iterable<TrainDeparture> {

  /**
   * An arraylist of train departures, as we do not know how many train departures there will be.
   * See a more in depth explanation of the choice of data structure in the report.
   */
  private final ArrayList<TrainDeparture> trainDepartures;


  /**
   * Creates a new train departure collection.
   */
  public TrainDepartureCollection() {
    this.trainDepartures = new ArrayList<>();
  }


  /**
   * Creates a new train departure collection with the given train departures from an array of
   * trainDepartures.
   *
   * @param trainDepartures the train departures to add to the collection
   */
  public TrainDepartureCollection(TrainDeparture[] trainDepartures) {
    this.trainDepartures = new ArrayList<>();
    for (TrainDeparture trainDeparture : trainDepartures) {
      this.addTrainDeparture(trainDeparture);
    }
  }

  public TrainDepartureCollection(Stream<TrainDeparture> trainDepartureStream) {
    this.trainDepartures = new ArrayList<>();
    trainDepartureStream.forEach(this::addTrainDeparture);
  }

  /**
   * Overrides the iterator method of the iterable interface, so that we can iterate over the object
   * directly.
   *
   * @return the iterator of the collection
   */
  @Override
  public Iterator<TrainDeparture> iterator() {
    return trainDepartures.iterator();
  }

  public Stream<TrainDeparture> stream() {
    return trainDepartures.stream();
  }

  /**
   * Gets the size of the collection.
   *
   * @return the size of the collection
   */
  public int size() {
    return trainDepartures.size();
  }

  /**
   * Adds a train departure to the collection.
   *
   * @param trainDeparture the train departure to add
   */
  public void addTrainDeparture(TrainDeparture trainDeparture) throws IllegalArgumentException {
    this.forEach(trainDeparture1 -> {
      if (trainDeparture1.getTrainNumber() == trainDeparture.getTrainNumber()) {
        throw new IllegalArgumentException("Train number already exists");
      }
    });
    this.trainDepartures.add(trainDeparture);
  }

  /**
   * Add a track to a train departure.
   *
   * @param trainNumber the train number of the train departure to edit
   * @param track       the track of the train departure
   */
  public void setTrackOfTrainDeparture(int trainNumber, int track) {
    //because the train number is unique, we can use it to identify the train departure to edit
    //as the setTrack method already has guards, we do not need to check if the track is valid
    if (!TrainDeparture.isTrack(String.valueOf(track))) {
      throw new IllegalArgumentException("Invalid track");
    }
    trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrainNumber() == trainNumber)
        .forEach(trainDeparture -> trainDeparture.setTrack(track));
  }


  /**
   * Add a delay to a train departure.
   *
   * @param trainNumber the train number of the train departure to edit
   * @param delay       the delay of the train departure
   */
  public void setDelayOfTrainDeparture(int trainNumber, String delay) {
    //because the train number is unique, we can use it to identify the train departure to edit
    trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrainNumber() == trainNumber)
        .forEach(trainDeparture -> trainDeparture.setDelay(delay));
  }

  /**
   * gets the train departures in the collection with the given destination.
   *
   * @param destination the destination of the train departures to get
   * @return a collection of train departures with the given destination
   */
  public TrainDepartureCollection getTrainDeparturesByDestination(String destination) {
    //get all train departures with the given destination
    return new TrainDepartureCollection(trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getDestination().equals(destination))
    );
  }


  /**
   * Gets the train departures in the collection sorted by time.
   *
   * @param ascending true if the train departures should be sorted ascending, false if descending
   * @return a collection of train departures sorted by time
   */
  public TrainDepartureCollection getDeparturesSortedByTime(boolean ascending) {
    return new TrainDepartureCollection(trainDepartures.stream()
        .sorted((trainDeparture1, trainDeparture2) -> {
          if (ascending) {
            return trainDeparture1.getDepartureTime()
                .compareTo(trainDeparture2.getDepartureTime());
          } else {
            return trainDeparture2.getDepartureTime()
                .compareTo(trainDeparture1.getDepartureTime());
          }
        }));
  }

  /**
   * Removes all train departures with a departure time before the given time.
   *
   * @param time the time to remove train departures before
   */
  public void removeTrainDeparturesBeforeTime(LocalTime time) {
    trainDepartures.removeIf(
        trainDeparture -> trainDeparture.getEstimatedDepartureTime().isBefore(time));
  }

  /**
   * Gets the train departures in the collection with the given train number.
   *
   * @param i the train number of the train departures to get
   * @return a collection of train departures with the given train number
   */
  public TrainDeparture getTrainDepartureByTrainNumber(int i) {
    return trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrainNumber() == i)
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("Train number does not exist"));
  }
}
