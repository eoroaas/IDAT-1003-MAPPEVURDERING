package edu.ntnu.stud.models;

import static java.lang.Integer.parseInt;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * This class represents a train departure.
 */
public class TrainDeparture {


  public static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
  private final int trainNumber;
  private final LocalTime departureTime;
  private final String destination;
  private final String line;
  private LocalTime delay;
  private int track;

  /**
   * Creates a new train departure.
   *
   * @param trainNumber   the train number
   * @param departureTime the departure time
   * @param delay         the delay
   * @param destination   the destination
   * @param line          the line
   * @param track         the track
   */
  public TrainDeparture(int trainNumber, String departureTime, String delay, String destination,
      String line,
      int track) {
    if (!isTrainNumber(String.valueOf(trainNumber))) {
      throw new IllegalArgumentException("Invalid train number");
    }
    if (!isTime(departureTime)) {
      throw new IllegalArgumentException("Invalid time format");
    }
    if (!isTime(delay)) {
      throw new IllegalArgumentException("Invalid time format");
    }
    if (!isDestination(destination)) {
      throw new IllegalArgumentException("Invalid destination");
    }
    if (!isLine(line)) {
      throw new IllegalArgumentException("Invalid line");
    }
    if (!isTrack(String.valueOf(track))) {
      throw new IllegalArgumentException("Invalid track number");
    }
    this.trainNumber = trainNumber;
    this.departureTime = LocalTime.parse(departureTime, timeFormatter);
    this.setDelay(delay);
    this.destination = destination;
    this.line = line;
    this.setTrack(track);
  }

  /**
   * Creates a new train departure, with the time as a LocalTime object.
   *
   * @param trainNumber   the train number
   * @param departureTime the departure time
   * @param delay         the delay
   * @param destination   the destination
   * @param line          the line
   */
  public TrainDeparture(int trainNumber, LocalTime departureTime, LocalTime delay,
      String destination,
      String line, int track) {
    if (!isTrainNumber(String.valueOf(trainNumber))) {
      throw new IllegalArgumentException("Invalid train number");
    }
    if (!isDestination(destination)) {
      throw new IllegalArgumentException("Invalid destination");
    }
    if (!isLine(line)) {
      throw new IllegalArgumentException("Invalid line");
    }

    this.trainNumber = trainNumber;
    this.departureTime = departureTime;
    this.setDelay(delay.format(timeFormatter));
    this.destination = destination;
    this.line = line;
    this.setTrack(track);
  }

  /**
   * Creates a new train departure, with the time as a LocalTime object, and without a track.
   *
   * @param trainNumber   the train number
   * @param departureTime the departure time
   * @param delay         the delay
   * @param destination   the destination
   * @param line          the line
   */
  public TrainDeparture(int trainNumber, LocalTime departureTime, LocalTime delay,
      String destination,
      String line) {
    if (!isTrainNumber(String.valueOf(trainNumber))) {
      throw new IllegalArgumentException("Invalid train number");
    }
    if (!isDestination(destination)) {
      throw new IllegalArgumentException("Invalid destination");
    }
    if (!isLine(line)) {
      throw new IllegalArgumentException("Invalid line");
    }

    this.trainNumber = trainNumber;
    this.departureTime = departureTime;
    this.setDelay(delay.format(timeFormatter));
    this.destination = destination;
    this.line = line;
    this.setTrack(-1);
  }


  /**
   * If a train departure has no track, the track is set to -1.
   */
  public TrainDeparture(int trainNumber, String departureTime, String delay, String destination,
      String line) {
    if (!isTrainNumber(String.valueOf(trainNumber))) {
      throw new IllegalArgumentException("Invalid train number");
    }
    if (!isTime(departureTime)) {
      throw new IllegalArgumentException("Invalid time format");
    }
    if (!isTime(delay)) {
      throw new IllegalArgumentException("Invalid time format");
    }
    if (!isDestination(destination)) {
      throw new IllegalArgumentException("Invalid destination");
    }
    if (!isLine(line)) {
      throw new IllegalArgumentException("Invalid line");
    }

    this.trainNumber = trainNumber;
    this.departureTime = LocalTime.parse(departureTime, timeFormatter);
    this.setDelay(delay);
    this.destination = destination;
    this.line = line;
    this.setTrack(-1);
  }

  /**
   * Checks if the input is an integer.
   *
   * @param input the input to check
   * @return true if the input is an integer, false otherwise
   */
  public static boolean isInteger(String input) {
    try {
      parseInt(input);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Checks if the input is a valid train number.
   *
   * @param input the input to check
   * @return true if the input is a valid train number, false otherwise
   */
  public static boolean isTrainNumber(String input) {

    return isInteger(input) && parseInt(input) > 0;
  }

  /**
   * Checks if the input is a valid line.
   *
   * @param input the input to check
   * @return true if the input is a valid line, false otherwise
   */
  public static boolean isLine(String input) {
    //no specific requirements
    return !input.isEmpty();
  }

  /**
   * Checks if the input is a valid track.
   *
   * @param input the input to check
   * @return true if the input is a valid track, false otherwise
   */
  public static boolean isTrack(String input) {
    //either a positive integer or -1
    return isInteger(input) && (Integer.parseInt(input) > 0 || Integer.parseInt(input) == -1);
  }

  /**
   * Checks if the input is a valid destination.
   *
   * @param input the input to check
   * @return true if the input is a valid destination, false otherwise
   */
  public static boolean isDestination(String input) {
    //no specific requirements
    return !input.isEmpty();
  }

  /**
   * Checks if the input is a valid time.
   *
   * @param input the input to check
   * @return true if the input is a valid time, false otherwise
   */
  public static boolean isTime(String input) {
    return input.matches("([01][0-9]|2[0-3]):[0-5][0-9]");

  }

  /**
   * Returns the train number.
   *
   * @return the train number
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Returns the departure time.
   *
   * @return the departure time
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }


  public LocalTime getEstimatedDepartureTime() {
    return departureTime.plusMinutes(delay.getMinute()).plusHours(delay.getHour());
  }

  public String getDepartureTimeAsString() {
    return departureTime.format(timeFormatter);
  }

  /**
   * Returns the delay.
   *
   * @return the delay
   */
  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Sets the delay.
   *
   * @param delay the delay
   */
  public void setDelay(String delay) {
    if (!isTime(delay)) {
      throw new IllegalArgumentException("Invalid time format");
    }

    this.delay = LocalTime.parse(delay, timeFormatter);
  }

  public String getDelayAsString() {
    return delay.format(timeFormatter);
  }

  /**
   * Returns the destination.
   *
   * @return the destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Returns the line.
   *
   * @return the line
   */
  public String getLine() {
    return line;
  }

  /**
   * Returns the track.
   *
   * @return the track
   */
  public int getTrack() {
    return track;
  }

  /**
   * Sets the track.
   *
   * @param track the track
   */
  public void setTrack(int track) {
    if (!isTrack(String.valueOf(track))) {
      throw new IllegalArgumentException("Invalid track number");
    }
    this.track = track;
  }
}
