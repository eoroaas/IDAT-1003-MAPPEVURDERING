package edu.ntnu.stud.userinterface;

import com.github.kwhat.jnativehook.GlobalScreen;
import com.github.kwhat.jnativehook.NativeHookException;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.models.TrainDeparture;
import edu.ntnu.stud.models.TrainDepartureCollection;
import java.time.LocalTime;
import java.util.Scanner;


/**
 * This class is responsible for the user interface and interaction with the user.
 */
public class UserInterface {

  public static Scanner inputScanner;


  /**
   * Initializes the user interface.
   */
  public static void init() {
    inputScanner = new Scanner(System.in);
    if (System.console() != null) {
      try {
        GlobalScreen.registerNativeHook();
      } catch (Exception ex) {
        System.err.println(
            "There was a problem registering the native hook, continuing without...");
      }
    }


  }

  /**
   * Adds a train departure and prompts the user for required info.
   */
  private static void addTrainDepartureWithPrompts() {
    boolean validTrainNumber = false;
    String trainNumber = "";
    while (!validTrainNumber) {
      try {
        trainNumber = new UiInputHandler(
            "Enter train number: ",
            "trainNumber").dialog();
        String finalTrainNumber = trainNumber;
        TrainDispatchApp.arrivingTrains.stream()
            .filter(train -> train.getTrainNumber() == Integer.parseInt(finalTrainNumber))
            .findFirst()
            .ifPresent(train -> {
              throw new IllegalArgumentException("Train number already exists");
            });
        validTrainNumber = true;
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }

    }

    String departureTime = new UiInputHandler(
        "Enter departure time (hh:mm): ",
        "time").dialog();

    String delay = new UiInputHandler(
        "Enter delay (hh:mm): ",
        "time").dialog();

    String destination = new UiInputHandler(
        "Enter destination: ",
        "destination").dialog();

    String line = new UiInputHandler(
        "Enter line: ",
        "line").dialog();

    String track = new UiInputHandler(
        "Enter track: ",
        "track").dialog();

    TrainDispatchApp.arrivingTrains.addTrainDeparture(
        new TrainDeparture(Integer.parseInt(trainNumber), departureTime, delay, destination, line,
            Integer.parseInt(track)));

  }

  /**
   * Starts the user interface.
   */
  public static void start() {
    int choice = -1;
    String[] choices = new String[]{"Add train departure", "List arriving trains",
        "Update the time",
        "Add track to train departure", "Add delay to train departure",
        "Get arriving trains by destination", "Get arriving train by train number",
        "Get trains sorted by departure time",
        "Quit"};

    while (choice != 8) {
      try {
        if (System.console() != null) {
          choice = new NativeEventUiMenuHandler(choices).dialog();
        } else {
          choice = new UiMenuHandler(choices).dialog();
        }
        switch (choice) {
          case 0 -> addTrainDepartureWithPrompts();
          case 1 -> displayArrivingTrains();
          case 2 -> setTimeWithPrompts();
          case 3 -> setTrackOfTrainDepartureWithPrompts();
          case 4 -> setDelayOfTrainDepartureWithPrompts();
          case 5 -> getTrainDeparturesByDestinationWithPrompts();
          case 6 -> getTrainDepartureByTrainNumberWithPrompts();
          case 7 -> getTrainDeparturesSortedByTimeWithPrompts();
          case 8 -> quit();
          default -> System.out.println("Invalid choice");
        }
      } catch (Exception e) {
        System.out.println(
            "An unexpected error occurred, please try again."); // failsafe
        System.out.println(e.getMessage());
      }
    }
  }

  private static void quit() {
    System.out.println("Quitting");
    if (System.console() != null) {
      try {
        GlobalScreen.unregisterNativeHook();
      } catch (NativeHookException e) {
        System.out.println("Failed to unregister native hook, close the terminal to quit.");
      }
    }
  }

  /**
   * Left pads a string with spaces.
   *
   * @param str the string to pad
   * @param n   the length of the resulting string
   * @return the padded string
   */
  private static String leftPad(String str, int n) {
    return String.format("%1$" + n + "s", str);

  }

  /**
   * Displays the arriving trains in a table.
   */
  private static void displayArrivingTrains() {

    displayTrainDepartures(TrainDispatchApp.arrivingTrains);

  }

  /**
   * Sets the time and prompts the user for required info.
   */
  private static void setTimeWithPrompts() {
    boolean validTime = false;
    //only allow time to be set forward
    while (!validTime) {
      try {
        String time = new UiInputHandler(
            "Enter time (hh:mm): ",
            "time").dialog();
        LocalTime newTime = LocalTime.parse(time);
        TrainDispatchApp.setCurrentTime(newTime);
        validTime = true;
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    }
  }

  /**
   * Sets the track of a train departure and prompts the user for required info.
   */
  private static void setTrackOfTrainDepartureWithPrompts() {
    String trainNumber = new UiInputHandler(
        "Enter train number: ",
        "trainNumber").dialog();

    String track = new UiInputHandler(
        "Enter track: ",
        "track").dialog();

    TrainDispatchApp.arrivingTrains.setTrackOfTrainDeparture(Integer.parseInt(trainNumber),
        Integer.parseInt(track));

  }

  /**
   * Sets the delay of a train departure and prompts the user for required info.
   */
  private static void setDelayOfTrainDepartureWithPrompts() {
    String trainNumber = new UiInputHandler(
        "Enter train number: ",
        "trainNumber").dialog();

    String delay = new UiInputHandler(
        "Enter delay: ",
        "time").dialog();

    TrainDispatchApp.arrivingTrains.setDelayOfTrainDeparture(Integer.parseInt(trainNumber), delay);

  }

  /**
   * Gets the train departures in the collection with the given destination and prompts the user for
   * required info.
   */
  public static void getTrainDeparturesByDestinationWithPrompts() {
    String destination = new UiInputHandler(
        "Enter destination: ",
        "destination").dialog();

    TrainDepartureCollection trainDeparturesByDestination = TrainDispatchApp.arrivingTrains
        .getTrainDeparturesByDestination(destination);
    displayTrainDepartures(trainDeparturesByDestination);

  }


  /**
   * Gets the train departures in the collection with the given train number and prompts the user
   * for required info.
   */
  public static void getTrainDepartureByTrainNumberWithPrompts() {
    String trainNumber = new UiInputHandler(
        "Enter train number: ",
        "trainNumber").dialog();

    displayTrainDepartures(new TrainDepartureCollection(TrainDispatchApp.arrivingTrains.stream()
        .filter(
            trainDeparture -> trainDeparture.getTrainNumber() == Integer.parseInt(trainNumber))));

  }

  /**
   * Gets the train departures in the collection sorted by time and prompts the user for required
   * info.
   */
  public static void getTrainDeparturesSortedByTimeWithPrompts() {
    String ascendingInput = new UiInputHandler(
        "Sort ascending? (y/n): ",
        "yesNo").dialog();
    boolean ascending = ascendingInput.equals("y");

    TrainDepartureCollection trainDeparturesSortedByTime = TrainDispatchApp.arrivingTrains
        .getDeparturesSortedByTime(ascending);
    displayTrainDepartures(trainDeparturesSortedByTime);

  }

  /**
   * Displays a collection of train departures in a table.
   *
   * @param trainDepartures the collection of train departures to display
   */
  public static void displayTrainDepartures(TrainDepartureCollection trainDepartures) {
    if (trainDepartures.size() == 0) {
      System.out.println("No trains found");
      return;
    }
    System.out.println("+-----+-----+-----+--------+----+-----+");
    System.out.println("|Train|Time |Delay|Dest    |Line|Track|");
    System.out.println("+-----+-----+-----+--------+----+-----+");
    trainDepartures.forEach(train ->
        System.out.println("|"
            + leftPad(Integer.toString(train.getTrainNumber()), 5)
            + "|"
            + leftPad(train.getDepartureTimeAsString(), 5)
            + "|"
            + leftPad(train.getDelayAsString(), 5)
            + "|"
            + leftPad(train.getDestination(), 8)
            + "|"
            + leftPad(train.getLine(), 4)
            + "|"
            + leftPad(Integer.toString(train.getTrack()), 5) + "|")
    );
    System.out.println("+-----+-----+-----+--------+----+-----+");

  }


}



