package edu.ntnu.stud.userinterface;

import com.github.kwhat.jnativehook.GlobalScreen;
import com.github.kwhat.jnativehook.keyboard.NativeKeyEvent;
import com.github.kwhat.jnativehook.keyboard.NativeKeyListener;
import java.util.Scanner;

/**
 * This class is responsible for the user interface menu, with native key listeners for a better ux
 * experience.
 */
public class NativeEventUiMenuHandler implements NativeKeyListener {

  //a normal input scanner to freeze the program while waiting for enter to be pressed
  private static final Scanner inputScanner = new Scanner(System.in);
  String[] menuItems;

  //the current choice
  private int currentChoice = 0;

  /**
   * Creates a new user interface menu handler.
   *
   * @param menuItems the menu items to print
   */
  public NativeEventUiMenuHandler(String[] menuItems) {
    this.menuItems = menuItems;
    GlobalScreen.addNativeKeyListener(this);
  }

  /**
   * Invoked when a key has been pressed.
   */
  public void nativeKeyPressed(NativeKeyEvent e) {
    if (e.getKeyCode() == NativeKeyEvent.VC_ENTER) {
      clearScreen();
      return;
    }
    if (e.getKeyCode() == NativeKeyEvent.VC_UP) {
      currentChoice--;
      if (currentChoice == -1) {
        currentChoice = menuItems.length - 1;
      }
    }
    if (e.getKeyCode() == NativeKeyEvent.VC_DOWN) {
      currentChoice++;
      if (currentChoice == menuItems.length) {
        currentChoice = 0;
      }
    }
    int[] keys = {NativeKeyEvent.VC_1, NativeKeyEvent.VC_2, NativeKeyEvent.VC_3,
        NativeKeyEvent.VC_4, NativeKeyEvent.VC_5, NativeKeyEvent.VC_6, NativeKeyEvent.VC_7,
        NativeKeyEvent.VC_8, NativeKeyEvent.VC_9};
    for (int i = 0; i < keys.length; i++) {
      if (e.getKeyCode() == keys[i] && i < menuItems.length) {
        currentChoice = i;
      }
    }
    clearScreen();
    this.printMenu();

  }

  /**
   * Takes care of the dialog between the user and the application when the user is prompted for a
   * menu choice.
   */
  public int dialog() {
    this.printMenu();
    inputScanner.nextLine();
    GlobalScreen.removeNativeKeyListener(this);
    System.out.println(currentChoice);
    return currentChoice;
  }

  /**
   * Prints out the menu with the current choice highlighted.
   */
  public void printMenu() {
    System.out.println(
        "Choose an option using the arrow keys, or a number, and use enter to select:");
    for (int i = 0; i < menuItems.length; i++) {
      if (i == currentChoice) {
        System.out.println(green((i + 1) + ". " + menuItems[i]));
      } else {
        System.out.println(gray((i + 1) + ". " + menuItems[i]));
      }
    }
  }

  /**
   * Color text green.
   */
  private String green(String s) {
    return "\033[32m" + s + "\033[0m";
  }

  /**
   * Color text gray.
   */
  private String gray(String s) {
    return "\033[90m" + s + "\033[0m";
  }


  /**
   * Clear the console screen.
   */
  private void clearScreen() {
    System.out.print("\033[H\033[2J");
    System.out.flush();
  }
}