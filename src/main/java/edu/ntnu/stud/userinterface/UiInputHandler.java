package edu.ntnu.stud.userinterface;

import edu.ntnu.stud.models.TrainDeparture;

/**
 * This class represents a user interface input.
 */
public class UiInputHandler {

  private final String prompt;
  private final String type;
  private int maxMenuChoice;


  /**
   * Creates a new user interface input handler.
   *
   * @param prompt the prompt to print
   * @param type   the type of input to accept
   */
  public UiInputHandler(String prompt, String type) {
    if (isNotValidPrompt(type)) {
      throw new IllegalArgumentException("Invalid prompt");
    }
    if (type.equals("menuChoice")) {
      throw new IllegalArgumentException("Must specify max menu choice");
    }
    this.prompt = prompt;
    this.type = type;


  }

  /**
   * Optionally a max menu choice for when the type is menuChoice.
   *
   * @param prompt        the prompt to print
   * @param type          the type of input to accept
   * @param maxMenuChoice the max menu choice
   */
  public UiInputHandler(String prompt, String type, int maxMenuChoice) {
    if (isNotValidPrompt(type)) {
      throw new IllegalArgumentException("Invalid prompt");
    }
    if (!type.equals("menuChoice")) {
      throw new IllegalArgumentException(
          "Must not specify max menu choice, when type is not menuChoice");
    }
    if (maxMenuChoice <= 0) {
      throw new IllegalArgumentException("Max menu choice must be greater than 0");
    }
    this.prompt = prompt;
    this.type = type;
    this.maxMenuChoice = maxMenuChoice;

  }

  private static boolean isNotValidPrompt(String prompt) {
    String[] validTypes = {"trainNumber", "line", "track", "destination", "time", "delay",
        "menuChoice", "yesNo"};
    return !java.util.Arrays.asList(validTypes).contains(prompt);
  }

  /**
   * Checks if the input is an integer.
   *
   * @param input the input to check
   * @return true if the input is an integer, false otherwise
   */
  private static boolean isInteger(String input) {
    try {
      Integer.parseInt(input);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Checks if the input is a valid yes or no.
   *
   * @param input the input to check
   * @return true if the input is a valid yes or no, false otherwise
   */
  private boolean isYesOrNo(String input) {
    return input.equals("y") || input.equals("n");
  }

  /**
   * Checks if the input is a valid menu choice.
   *
   * @param input the input to check
   * @return true if the input is a valid menu choice, false otherwise
   */
  private boolean isMenuChoice(String input) {
    return isInteger(input) && Integer.parseInt(input) > 0
        && Integer.parseInt(input) <= this.maxMenuChoice;
  }

  /**
   * Gets the accepted input.
   *
   * @return the accepted input
   */
  private String getAcceptedInput() {
    String input = null; //initialize to null to avoid compiler error
    boolean isInputValid = false;
    while (!isInputValid) {
      input = UserInterface.inputScanner.nextLine();
      //could have been simplified to a huge if statement, but this is more readable
      if (type.equals("trainNumber") && TrainDeparture.isTrainNumber(
          input)) {
        isInputValid = true;
      } else if (type.equals("line") && TrainDeparture.isLine(input)) {
        isInputValid = true;
      } else if (type.equals("track") && (TrainDeparture.isTrack(input) || input.isEmpty())) {
        isInputValid = true;
      } else if (type.equals("destination") && TrainDeparture.isDestination(input)) {
        isInputValid = true;
      } else if (type.equals("time") && TrainDeparture.isTime(input)) {
        isInputValid = true;
      } else if (type.equals("delay") && (TrainDeparture.isTime(input) || input.isEmpty())) {
        isInputValid = true;
      } else if (type.equals("menuChoice") && isMenuChoice(input)) {
        isInputValid = true;
      } else if (type.equals("yesNo") && isYesOrNo(input)) {
        isInputValid = true;
      } else {
        System.out.print("Invalid input, try again:");
      }
    }
    return input;
  }

  /**
   * Prints the prompt.
   */
  private void printPrompt() {
    System.out.print(prompt);
  }

  public String dialog() {
    printPrompt();
    return getAcceptedInput();
  }


}
