package edu.ntnu.stud.userinterface;

/**
 * This class represents a user interface input handler.
 */
public class UiMenuHandler {

  String[] menuItems;

  /**
   * Creates a new user interface menu handler.
   *
   * @param menuItems the menu items to print
   */
  public UiMenuHandler(String[] menuItems) {
    if (menuItems.length == 0) {
      throw new IllegalArgumentException("Must have at least one menu item");
    }
    this.menuItems = menuItems;
  }

  /**
   * Prints the menu.
   */
  private void printMenu() {
    for (int i = 0; i < menuItems.length; i++) {
      System.out.println((i + 1) + ". " + menuItems[i]);
    }
  }

  /**
   * Gets a valid menu choice from the user.
   *
   * @return the accepted menu choice
   */
  private int getAcceptedMenuChoice() {
    int choice = Integer.parseInt(new UiInputHandler("Enter choice: ", "menuChoice",
        menuItems.length).dialog());

    return choice - 1;
  }

  public int dialog() {
    printMenu();
    return getAcceptedMenuChoice();
  }
}
