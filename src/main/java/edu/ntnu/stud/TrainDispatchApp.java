package edu.ntnu.stud;

import edu.ntnu.stud.models.TrainDeparture;
import edu.ntnu.stud.models.TrainDepartureCollection;
import edu.ntnu.stud.userinterface.UserInterface;
import java.time.LocalTime;

/**
 * This is the main class for the train dispatch application.
 */
public class TrainDispatchApp {

  public static TrainDepartureCollection arrivingTrains = new TrainDepartureCollection();
  private static LocalTime currentTime = LocalTime.of(0, 0);

  /**
   * Gets the current time.
   *
   * @return the current time
   */
  public static LocalTime getCurrentTime() {
    return currentTime;
  }

  /**
   * Sets the current time.
   *
   * @param currentTime the current time
   */
  public static void setCurrentTime(LocalTime currentTime) throws IllegalArgumentException {
    // only allow setting the time if it is after the current time
    if (currentTime.isAfter(TrainDispatchApp.currentTime)) {
      TrainDispatchApp.currentTime = currentTime;
      TrainDispatchApp.arrivingTrains.removeTrainDeparturesBeforeTime(
          TrainDispatchApp.currentTime);
    } else {
      throw new IllegalArgumentException("Time must be after current time");
    }
  }


  /**
   * The main method.
   */
  public static void main(String[] args) {
    arrivingTrains.addTrainDeparture(new TrainDeparture(1, "12:00", "00:00", "Oslo", "L1", 1));
    arrivingTrains.addTrainDeparture(new TrainDeparture(2, "12:00", "00:00", "Oslo", "L1", 2));
    arrivingTrains.addTrainDeparture(new TrainDeparture(3, "12:00", "00:00", "Oslo", "L1", 3));

    UserInterface.init();
    UserInterface.start();
  }
}
