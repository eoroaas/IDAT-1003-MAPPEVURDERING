package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.userinterface.UiInputHandler;
import org.junit.jupiter.api.Test;

class UiInputHandlerTest {

  @Test
  void constructor() {
    new UiInputHandler("test", "trainNumber");
    new UiInputHandler("test", "line");
    new UiInputHandler("test", "track");
    new UiInputHandler("test", "destination");
    new UiInputHandler("test", "time");
    new UiInputHandler("test", "delay");
    new UiInputHandler("test", "menuChoice", 2);
    new UiInputHandler("test", "yesNo");
    //negative tests
    assertThrows(IllegalArgumentException.class, () -> new UiInputHandler("test", "test"));
    assertThrows(IllegalArgumentException.class, () -> new UiInputHandler("test", "menuChoice"));
    assertThrows(IllegalArgumentException.class, () -> new UiInputHandler("test", "menuChoice", 0));

  }


  @Test
  void dialog() {
    //as this is a prompt based method, it is not possible to test fully with the Junit class
  }
}