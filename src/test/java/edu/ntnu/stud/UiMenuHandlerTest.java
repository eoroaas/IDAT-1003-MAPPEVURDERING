package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.userinterface.UiMenuHandler;
import org.junit.jupiter.api.Test;

class UiMenuHandlerTest {

  @Test
  void constructor() {
    new UiMenuHandler(new String[]{"test"});

    //negative tests
    //no menu items
    assertThrows(IllegalArgumentException.class, () -> new UiMenuHandler(new String[]{}));

  }


  @Test
  void dialog() {
    //is console based, not possible to test
  }
}