package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.stud.models.TrainDeparture;
import edu.ntnu.stud.models.TrainDepartureCollection;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

class TrainDepartureCollectionTest {

  @Test
  void iterator() {
    assertFalse(new TrainDepartureCollection().iterator().hasNext());
    assertTrue(new TrainDepartureCollection(new TrainDeparture[]{
        new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1)}).iterator().hasNext());

  }

  @Test
  void addTrainDeparture() {
    TrainDepartureCollection trainDepartureCollection = new TrainDepartureCollection();
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1));
    int count = 0;
    for (TrainDeparture trainDeparture : trainDepartureCollection) {
      count++;
      assertEquals(1234, trainDeparture.getTrainNumber());
      assertEquals("12:34", trainDeparture.getDepartureTimeAsString());
      assertEquals("12:34", trainDeparture.getDelayAsString());
      assertEquals("Oslo S", trainDeparture.getDestination());
      assertEquals("L1", trainDeparture.getLine());
      assertEquals(1, trainDeparture.getTrack());
    }
    assertEquals(1, count);
    //test if it throws exception when adding a train with the same number
    assertThrows(IllegalArgumentException.class, () -> trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1)));

  }

  @Test
  void setTrackOfTrainDeparture() {
    TrainDepartureCollection trainDepartureCollection = new TrainDepartureCollection();
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.setTrackOfTrainDeparture(1234, 2);
    assertEquals(2, trainDepartureCollection.iterator().next().getTrack());
    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureCollection.setTrackOfTrainDeparture(
            1234, 0));
  }

  @Test
  void setDelayOfTrainDeparture() {
    TrainDepartureCollection trainDepartureCollection = new TrainDepartureCollection();
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.setDelayOfTrainDeparture(1234, "00:01");
    assertEquals("00:01", trainDepartureCollection.iterator().next().getDelayAsString());

  }

  @Test
  void getTrainDeparturesByDestination() {
    TrainDepartureCollection trainDepartureCollection = new TrainDepartureCollection();
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1235, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1236, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1237, "12:34", "12:34", "Mjøndalen", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1238, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1239, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1240, "12:34", "12:34", "Asker S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1241, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1242, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1243, "12:34", "12:34", "Oslo S", "L1", 1));
    TrainDepartureCollection trainDepartures = trainDepartureCollection.getTrainDeparturesByDestination(
        "Oslo S");
    for (TrainDeparture trainDeparture : trainDepartures) {
      assertEquals("Oslo S", trainDeparture.getDestination());
    }
    assertEquals(8, trainDepartures.size());
    assertEquals(0,
        trainDepartureCollection.getTrainDeparturesByDestination("Kristiansand").size());
  }

  @Test
  void getTrainDepartureByTrainNumber() {
    TrainDepartureCollection trainDepartureCollection = new TrainDepartureCollection();
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1235, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1236, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1237, "12:34", "12:34", "Mjøndalen", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1238, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1239, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1240, "12:34", "12:34", "Asker S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1241, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1242, "12:34", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1243, "12:34", "12:34", "Oslo S", "L1", 1));
    TrainDeparture trainDeparture = trainDepartureCollection.getTrainDepartureByTrainNumber(1237);
    assertEquals(1237, trainDeparture.getTrainNumber());
    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureCollection.getTrainDepartureByTrainNumber(123));

  }

  @Test
  void getDeparturesSortedByTime() {
    TrainDepartureCollection trainDepartureCollection = new TrainDepartureCollection();
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1234, "12:34", "12:35", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1235, "12:35", "12:34", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1236, "12:36", "12:36", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1237, "12:37", "12:37", "Mjøndalen", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1238, "12:38", "12:38", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1239, "12:39", "12:39", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1240, "12:40", "12:40", "Asker S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1241, "12:41", "12:41", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1242, "12:42", "12:42", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1243, "12:43", "12:43", "Oslo S", "L1", 1));
    TrainDepartureCollection trainDepartures = trainDepartureCollection.getDeparturesSortedByTime(
        true);
    List<TrainDeparture> trainDepartureList = new ArrayList<>();
    for (TrainDeparture trainDeparture : trainDepartures) {
      trainDepartureList.add(trainDeparture);
    }
    assertEquals("12:34", trainDepartureList.get(0).getDepartureTimeAsString());
    assertEquals("12:35", trainDepartureList.get(1).getDepartureTimeAsString());
    assertEquals("12:36", trainDepartureList.get(2).getDepartureTimeAsString());
    assertEquals("12:37", trainDepartureList.get(3).getDepartureTimeAsString());
    assertEquals("12:38", trainDepartureList.get(4).getDepartureTimeAsString());
    assertEquals("12:39", trainDepartureList.get(5).getDepartureTimeAsString());
    assertEquals("12:40", trainDepartureList.get(6).getDepartureTimeAsString());
    assertEquals("12:41", trainDepartureList.get(7).getDepartureTimeAsString());
    assertEquals("12:42", trainDepartureList.get(8).getDepartureTimeAsString());
    assertEquals("12:43", trainDepartureList.get(9).getDepartureTimeAsString());
    trainDepartures = trainDepartureCollection.getDeparturesSortedByTime(
        false);
    trainDepartureList = new ArrayList<>();
    for (TrainDeparture trainDeparture : trainDepartures) {
      trainDepartureList.add(trainDeparture);
    }
    assertEquals("12:43", trainDepartureList.get(0).getDepartureTimeAsString());
    assertEquals("12:42", trainDepartureList.get(1).getDepartureTimeAsString());
    assertEquals("12:41", trainDepartureList.get(2).getDepartureTimeAsString());
    assertEquals("12:40", trainDepartureList.get(3).getDepartureTimeAsString());
    assertEquals("12:39", trainDepartureList.get(4).getDepartureTimeAsString());
    assertEquals("12:38", trainDepartureList.get(5).getDepartureTimeAsString());
    assertEquals("12:37", trainDepartureList.get(6).getDepartureTimeAsString());
    assertEquals("12:36", trainDepartureList.get(7).getDepartureTimeAsString());
    assertEquals("12:35", trainDepartureList.get(8).getDepartureTimeAsString());
    assertEquals("12:34", trainDepartureList.get(9).getDepartureTimeAsString());
  }

  @Test
  void removeTrainDeparturesBeforeTime() {
    TrainDepartureCollection trainDepartureCollection = new TrainDepartureCollection();
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1234, "12:34", "00:00", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1235, "12:35", "00:00", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1236, "12:36", "00:00", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1237, "12:37", "00:00", "Mjøndalen", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1238, "12:38", "00:05", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1239, "12:39", "00:00", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1240, "12:40", "00:00", "Asker S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1241, "12:41", "00:00", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1242, "12:42", "00:00", "Oslo S", "L1", 1));
    trainDepartureCollection.addTrainDeparture(
        new TrainDeparture(1243, "12:43", "00:00", "Oslo S", "L1", 1));
    trainDepartureCollection.removeTrainDeparturesBeforeTime(LocalTime.of(12, 40));
    assertEquals(5, trainDepartureCollection.size());
    for (TrainDeparture trainDeparture : trainDepartureCollection) {
      System.out.println(trainDeparture.getEstimatedDepartureTime());
      assertTrue(trainDeparture.getEstimatedDepartureTime()
          .isAfter(LocalTime.of(12, 39)));//12:39 is the last time before 12:40
    }
  }
}