package edu.ntnu.stud;

import edu.ntnu.stud.models.TrainDeparture;
import edu.ntnu.stud.models.TrainDepartureCollection;
import edu.ntnu.stud.userinterface.UserInterface;
import org.junit.jupiter.api.Test;

class UserInterfaceTest {

  @Test
  void init() {
    UserInterface.init();
  }

  @Test
  void start() {
    //prompt based
  }

  @Test
  void getTrainDeparturesByDestinationWithPrompts() {
    //prompt based

  }

  @Test
  void getTrainDeparturesByTrainNumberWithPrompts() {
    //prompt based
  }

  @Test
  void getTrainDeparturesSortedByTimeWithPrompts() {
    //prompt based
  }

  @Test
  void displayTrainDepartures() {
    UserInterface.displayTrainDepartures(
        new TrainDepartureCollection(new TrainDeparture[]{
            new TrainDeparture(1234, "12:34", "00:00", "Oslo S", "L1", 1),
        }));
  }
}