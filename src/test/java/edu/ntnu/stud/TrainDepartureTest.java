package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.stud.models.TrainDeparture;
import java.time.LocalTime;
import org.junit.jupiter.api.Test;

class TrainDepartureTest {

  /**
   * Test the constructor with valid input.
   */
  @Test
  public void trainDepartureConstructorTest() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "00:00", "Oslo S", "L1", 1);
    TrainDeparture trainDeparture1 = new TrainDeparture(1234, LocalTime.of(0, 0, 0),
        LocalTime.of(0, 0, 0), "Oslo S", "L1", 1);
    TrainDeparture trainDeparture2 = new TrainDeparture(1234, "12:34", "00:30", "Oslo S", "L1");
    TrainDeparture trainDeparture3 = new TrainDeparture(1234, LocalTime.of(0, 0, 0),
        LocalTime.of(0, 0, 0), "Oslo S", "L1");
    //tests
    assertEquals(1234, trainDeparture.getTrainNumber());
    assertEquals(LocalTime.of(12, 34), trainDeparture.getDepartureTime());
    assertEquals(LocalTime.of(12, 34), trainDeparture.getEstimatedDepartureTime());
    assertEquals(LocalTime.of(0, 0), trainDeparture.getDelay());
    assertEquals("00:00", trainDeparture.getDelayAsString());
    assertEquals("12:34", trainDeparture.getDepartureTimeAsString());
    assertEquals("Oslo S", trainDeparture.getDestination());
    assertEquals("L1", trainDeparture.getLine());
    assertEquals(1, trainDeparture.getTrack());
    assertEquals(1234, trainDeparture1.getTrainNumber());
    assertEquals(LocalTime.of(0, 0), trainDeparture1.getDepartureTime());
    assertEquals(LocalTime.of(0, 0), trainDeparture1.getEstimatedDepartureTime());
    assertEquals(LocalTime.of(0, 0), trainDeparture1.getDelay());
    assertEquals("00:00", trainDeparture1.getDelayAsString());
    assertEquals("00:00", trainDeparture1.getDepartureTimeAsString());
    assertEquals("Oslo S", trainDeparture1.getDestination());
    assertEquals("L1", trainDeparture1.getLine());
    assertEquals(1, trainDeparture1.getTrack());
    assertEquals(1234, trainDeparture2.getTrainNumber());
    assertEquals(LocalTime.of(12, 34), trainDeparture2.getDepartureTime());
    assertEquals(LocalTime.of(13, 4), trainDeparture2.getEstimatedDepartureTime());
    assertEquals(LocalTime.of(0, 30), trainDeparture2.getDelay());
    assertEquals("00:30", trainDeparture2.getDelayAsString());
    assertEquals("12:34", trainDeparture2.getDepartureTimeAsString());
    assertEquals("Oslo S", trainDeparture2.getDestination());
    assertEquals("L1", trainDeparture2.getLine());
    assertEquals(-1, trainDeparture2.getTrack());
    assertEquals(1234, trainDeparture3.getTrainNumber());
    assertEquals(LocalTime.of(0, 0), trainDeparture3.getDepartureTime());
    assertEquals(LocalTime.of(0, 0), trainDeparture3.getEstimatedDepartureTime());
    assertEquals(LocalTime.of(0, 0), trainDeparture3.getDelay());
    assertEquals("00:00", trainDeparture3.getDelayAsString());
    assertEquals("00:00", trainDeparture3.getDepartureTimeAsString());
    assertEquals("Oslo S", trainDeparture3.getDestination());
    assertEquals("L1", trainDeparture3.getLine());
    assertEquals(-1, trainDeparture3.getTrack());

  }

  @Test
  void getTrainNumber() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1);
    assertEquals(1234, trainDeparture.getTrainNumber());
  }

  @Test
  void getDepartureTime() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1);
    assertEquals(LocalTime.of(12, 34), trainDeparture.getDepartureTime());
  }

  @Test
  void getEstimatedDepartureTime() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "00:34", "Oslo S", "L1", 1);
    assertEquals(LocalTime.of(13, 8), trainDeparture.getEstimatedDepartureTime());
  }

  @Test
  void getDepartureTimeAsString() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1);
    assertEquals("12:34", trainDeparture.getDepartureTimeAsString());

  }

  @Test
  void getDelay() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1);
    assertEquals(LocalTime.of(12, 34), trainDeparture.getDelay());
  }

  @Test
  void getDelayAsString() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1);
    assertEquals("12:34", trainDeparture.getDelayAsString());
  }

  @Test
  void setDelay() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1);

    trainDeparture.setDelay("01:01");
    assertEquals(LocalTime.of(1, 1), trainDeparture.getDelay());
    assertThrows(IllegalArgumentException.class,
        () -> trainDeparture.setDelay("24:00"));
    assertThrows(IllegalArgumentException.class,
        () -> trainDeparture.setDelay("12:34:00"));
    assertThrows(IllegalArgumentException.class,
        () -> trainDeparture.setDelay("12:34a"));
  }

  @Test
  void getDestination() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1);

    assertEquals("Oslo S", trainDeparture.getDestination());
  }

  @Test
  void getLine() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 1);

    assertEquals("L1", trainDeparture.getLine());
  }

  @Test
  void getTrack() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1", 2);
    assertEquals(2, trainDeparture.getTrack());
  }

  @Test
  void setTrack() {
    TrainDeparture trainDeparture = new TrainDeparture(1234, "12:34", "12:34", "Oslo S", "L1");
    trainDeparture.setTrack(2);
    assertEquals(2, trainDeparture.getTrack());

  }

  @Test
  void isInteger() {
    assertTrue(TrainDeparture.isInteger("1234"));
    assertFalse(TrainDeparture.isInteger("1234a"));

  }

  @Test
  void isTrainNumber() {
    assertTrue(TrainDeparture.isTrainNumber("1234"));
    assertFalse(TrainDeparture.isTrainNumber("1234a"));
  }

  @Test
  void isLine() {
    assertTrue(TrainDeparture.isLine("L1"));
    assertFalse(TrainDeparture.isLine(""));
  }

  @Test
  void isTrack() {
    assertTrue(TrainDeparture.isTrack("1"));
    assertFalse(TrainDeparture.isTrack("1a"));
  }

  @Test
  void isDestination() {
    assertTrue(TrainDeparture.isDestination("Oslo S"));
    assertFalse(TrainDeparture.isDestination(""));
  }

  @Test
  void isTime() {
    assertTrue(TrainDeparture.isTime("12:34"));
    assertFalse(TrainDeparture.isTime("12:34a"));
    assertFalse(TrainDeparture.isTime("12:34:00"));
    assertFalse(TrainDeparture.isTime("24:00"));
  }
}