package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalTime;
import org.junit.jupiter.api.Test;

class TrainDispatchAppTest {


  @Test
  void currentTimeGetAndSet() {
    TrainDispatchApp.setCurrentTime(LocalTime.of(1, 0));
    assertEquals(LocalTime.of(1, 0), TrainDispatchApp.getCurrentTime());
    assertNotEquals(LocalTime.of(0, 0), TrainDispatchApp.getCurrentTime());

  }


}