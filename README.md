# Portfolio project IDATA1003 - 2023

STUDENT NAME = Embret Olav Rasmussen Roås  
STUDENT ID = 10080

## Project description

The repository contains a Java project for the final project in the course IDATA1003 - 2023.
The project is a simple, text-based program that can be used to manage a collection of train
departures.
The program can be used to add new departures, list departures and search, sort and filter
departures. The program also keeps an internal time value, and deletes departures that have passed.

## Project structure

The project is structured as a Maven project, with the following packages:

- UserInterface, where all user interaction is handled.
- Models, where the data models, that represent the data in the program, are located.

## Link to repository

https://gitlab.stud.idi.ntnu.no/eoroaas/IDAT-1003-MAPPEVURDERING

## How to run the project

The project can be run by running the main method in the class `TrainDispatchApp.java`.
That can be done by running the following command in the terminal from the root directory of the
project:

    mvn install
    mvn exec:java 

The program can also be run by running the main class through an IDE like IntelliJ or Eclipse, but
the program will use the old menu system, and not the new one, as this needs a console to run.

## How to run the tests

The tests can be run by running the following command in the terminal from the root directory of
the project:

    mvn test

